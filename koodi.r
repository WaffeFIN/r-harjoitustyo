################################################################################
#  R Harjoitustyö
#  
#
################################################################################
# TYHJENNYS & ALUSTUKSET
rm(list=ls())
options(scipen = 20)

# KORVAA ALLA OLEVA PATHI OMALLA
#esim. path <- '~/BitBucket/r-harjoitustyo/'

path <- ''

asiakastyytyvaisyys_path <- paste0(path, 'asiakastyytyvaisyys.csv')
yk_path <- paste0(path, 'YK.csv')

################################################################################
# Tehtävä 1

asty <- read.csv2(asiakastyytyvaisyys_path, stringsAsFactors = FALSE)

yhteenveto <- function(muuttuja) {
  c(frekvenssitaulu=table(cut(x = muuttuja, breaks = seq(from = 0.5, to = 5.5, length.out = 6))), # |||||| fix
  n=sum(!is.na(muuttuja)),
  na=sum(is.na(muuttuja)),
  keskiarvo=mean(muuttuja, na.rm = TRUE),
  varianssi=var(muuttuja, na.rm = TRUE))
}

summamuuttuja <- function(muuttujat, taulukko, max_puuttuvat=1) {
  apply(subset(taulukko, select = muuttujat), 1, function(row) { # ei ihan niin funktionaalinen kuin pitäisi, mut meh, toimii
    if (sum(is.na(row)) > max_puuttuvat)
      NA
    else
      mean(row, na.rm = TRUE)
  })
}

asty$tyyt_kulj<-summamuuttuja(c('K1A1', 'K1A2', 'K1A3'), taulukko=asty)
hist(asty$tyyt_kulj, col="red")
hist(asty$tyyt_kulj, col="green", breaks=8)
ka<-mean(asty$tyyt_kulj, na.rm = TRUE)
abline(v=ka, lwd=5, col="orange")

asty$tyyt_hsl<-summamuuttuja(c('K1A4', 'K1A5', 'K1A6','K2A2','K2A3','K2A4', 'K2A5', 'K2A6'), taulukko=asty, max_puuttuvat=5)
teht1_yhteenveto <- yhteenveto(asty$tyyt_hsl)
teht1_yhteenveto[1:5]
teht1_yhteenveto["keskiarvo"]

################################################################################
# Tehtävä 2

asty_osa <- asty[asty$T18 %in% c(200,2100,2230), ]
fact <- factor(asty_osa$T18, levels=c(200, 2100, 2230), labels = c('Lauttasaari','Tapiola','Matinkylä'))

asty_osa_risti <- table(fact, asty_osa$K3A23)
asty_osa_risti_prop <- prop.table(asty_osa_risti, 1)
apply(asty_osa_risti_prop, MARGIN=1:2, FUN = function(x) round(x, 3))

chisq.test(asty_osa_risti, p=0.01) #toimiiko oikein?

asty_osa_risti_lauttasaari <- asty_osa_risti[1,]
asty_osa_risti_lauttasaari <- asty_osa$K3A23[which(asty_osa$T18 == 200)]
asty_osa_risti_lauttasaari <- asty_osa_risti_lauttasaari[!is.na(asty_osa_risti_lauttasaari)]
t.test(asty_osa_risti_lauttasaari, mu = 3, conf.level = 0.95, alternative = "greater")
t.test(asty_osa_risti_lauttasaari, mu = 3, conf.level = 0.95, alternative = "less")

################################################################################
# Tehtävä 3

asty$vuosi <- as.numeric(format(as.Date(asty$PAIVAMAARA,"%Y-%m-%d"), "%Y"))
asty$ika <- asty$vuosi - asty$T7

KeskiarvoJaVali <- function(luvut, luottamus=0.95) {
  n=length(luvut)
  virhe=qt(luottamus, df=n-1)*sd(luvut)/sqrt(n)
  ka=mean(luvut, rm.na=TRUE)
  c(ka, ka-virhe, ka+virhe)
}

ikaluokat <- c(0,18,30,40,66,75,200)
asty$ikaluokka <- cut(asty$ika, breaks=ikaluokat, labels=FALSE)
asty_metro <- asty[which(asty$LIIKENNEMUOTO==3),] #metro = 3

teht3_vastausmatriisi = matrix(nrow = 3, ncol = 6)
for(luokka in 1:6) { # ruma mutta toimiva
  temp <- asty_metro[which(asty_metro$ikaluokka==luokka),]
  temp <- temp[!is.na(temp$K1A6),]
  temp <- temp$K1A6
  teht3_vastausmatriisi[, luokka] <- KeskiarvoJaVali(temp)
}
paste('Ensimmäinen rivi on keskiarvo, toinen ja kolmas rivi määrittävät 95% luottamusvälin')
teht3_vastausmatriisi

frame()
plot.window(xlim=c(1,6), ylim=c(1, 5))
axis(1, at=c(1, 2, 3, 4, 5, 6), labels = c("-17","18-29","30-39","40-65","66-74","75+"), lwd=2)
axis(2, at=c(1, 2, 3, 4, 5), lwd=2)
box(lwd=2)
title("Metrokäyttäjien K1A6 ikäryhmittäin")
points(x = teht3_vastausmatriisi[1,])
segments(c(1, 2, 3, 4, 5, 6), teht3_vastausmatriisi[2,], y1 = teht3_vastausmatriisi[3,])

################################################################################
# Tehtävä 4

yk <- read.csv2(yk_path, stringsAsFactors = FALSE)
yk$logBKT <- log(yk$BKT)
yk$alue_vari <- factor(yk$alue, levels=c("Africa","Europe","Asia","Caribbean","Latin America","Oceania"))

teht_4_plotNfit <- function(combo, nimi1, nimi2) {
  fit <- lm(combo)
  plot(combo, col = yk$alue_vari, xlab=nimi1, ylab=nimi2)
  abline(fit)
  legend(x="bottomleft", legend = levels(yk$alue_vari), col=yk$alue_vari, pch = 1, cex = 0.7)
  print(summary(fit))
}
teht_4_plotNfit(yk$vaestonkasvu~yk$logBKT, "Väestönkasvu %", "log(BKT)")
text(11.2, 2.8, "Brunei", cex = .8)
text(6, 3.4, "Qatar", cex = .8)

teht_4_plotNfit(yk$vaestonkasvu~yk$lukutaito_naiset, "Väestönkasvu %", "Naisten lukutaito")
confint(lm(yk$vaestonkasvu~yk$logBKT+yk$lukutaito_naiset), level = 0.9)

################################################################################
# Tehtävä 5

simulointi_pekka <- function(n) {
  palaute <- seq.default(from = 0, to = 1, length.out = n)
  simulointi_apu <- Vectorize(simulointi_apu)
  palaute[simulointi_apu(palaute, 6, 4)]
}

simulointi_apu <- function(theta, n, tulos) {
  sum(sample(c(0,1), size = n, replace = TRUE, prob = c(1-theta, theta))) == tulos
}

tulos_pekka <- simulointi_pekka(100000)
hist(tulos_pekka, breaks = 10)
tulos_pekka_kv <- KeskiarvoJaVali(tulos_pekka)
paste0('Keskiarvo: ', round(tulos_pekka_kv[1],3 ))
paste0('95% Luottamusväli: [', round(tulos_pekka_kv[2], 3), '-', round(tulos_pekka_kv[3],  3),']')

sd(tulos_pekka)


simulointi_maija <- function(n) {
  palaute <- sample(c(0.3,0.5,0.6,0.8), size = n, replace = TRUE, prob = c(0.25, 0.25, 0.25, 0.25))
  simulointi_apu <- Vectorize(simulointi_apu)
  palaute[simulointi_apu(palaute, 6, 4)]
}

tulos_maija <- simulointi_maija(100000)
hist(tulos_maija, breaks = 10)

print("Kiitos moi!")
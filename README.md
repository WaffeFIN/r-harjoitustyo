# Tilastollinen päättely R-ohjelmistolla 2017, Harjoitustyö #
## Ohjeet ##
Alla on annettu viisi tehtävää; vastaa kaikkiin näistä. Vastaus koostuu pyydetyistä
kuvista, taulukoista ja sanallisista selityksistä. Kokoa kaikkien tehtävien
vastaukset yhdeksi raportiksi, jonka voit kirjoittaa haluamallasi tekstinkäsittelyohjelmalla,
esim. Wordilla tai LATEX:illa, ja palauta raportti kurssin Moodlealueelle
.pdf -muodossa perjantaihin 26.5 klo 23.55 mennessä. Ohjelmakoodia
raportissa ei tarvitse olla, mutta osa tuloksista voi olla kätevintä lisätä raporttiin
suoraan ohjelman tulosteena.

Palauta lisäksi koodi, jota käytit tehtävien tekemiseen, erillisenä .R-tiedostona.
Kaikkien kysyttyjen tulosten täytyy tällä kertaa löytyä myös raportista: jos esimerkiksi
kysytään keskiarvoa, kirjoita se näkyviin raporttiin. Tai jos pyydetään
piirtämään kuva, liitä se raporttiin. Muista siis raportoida kaikki, mitä tehtä-
vissä kysytään.

Palautukseen kuuluu siis 2 tiedostoa:

1. Raportti pdf-tiedostona.
2. R-koodi .R-tiedostona

Huomaa: Tehtävämonisteesta on olemassa useampi eri versio. Tehtävät on
jaettu Moodlessa opiskelijanumeron mukaan niin, että kaikki eivät saa samaa
tehtävämonistetta. Älä siis vastaa kaverin tehtävämonisteen tehtäviin, vaan hae
tehtävät kirjautuneena sisään omilla tunnuksillasi!
Harjoitustyön tarkoitus on testata kurssin aikana opittujen asioiden osaamista
itsenäisesti, joten harjoitustyön tehtävistä ei voi valitettavasti enää keskustella
Moodlessa eikä Presemossa.

Plagiarismi ja opintovilppi, eli esimerkiksi kaverilta saatujen vastausten kopiointi
ja niiden palauttaminen omana työnä on harjoitustyössä kiellettyä. Palautettuihin
harjoitustöihin voidaan käyttää Urkund-plagiaatintunnistusjärjestelmää
ja todettu opintovilppi johtaa kurssisuorituksen hylkäämiseen.

## Aineistosta ##

Aineistona tehtävissä 1-3 on HSL:n asiakastyytyväisyyskysely. Aineistoon on valittu
vastaukset ajalta 1.8.2016-17.2.2017. Tehtävässä 4 käsitellään vuorostaan
osaa YK:n sosiaali-indikaattorit-aineistosta. Molemmat aineistot ovat saatavilla
kurssin Moodle-alueella.

## Tehtävät ##
### 1: Lista, summamuuttuja ja yhden muuttujan jakaumien tarkastelua ###

Kirjoita funktio yhteenveto, joka ottaa argumentikseen tarkasteltavan muuttujan. Funktion tulee palauttaa lista, jossa on seuraavat viisi komponenttia:

* argumenttina annetun muuttujan frekvenssitaulu
* argumenttina annetun muuttujan ei-puuttuvien arvojen määrä
* argumenttina annetun muuttujan puuttuvien arvojen määrä
* argumenttina annetun muuttujan keskiarvo
* argumenttina annetun muuttujan varianssi
	
Anna listan komponenteille kuvaavat nimet. Varmista, että keskiarvoon
ja varianssiin ei lasketa puuttuvia arvoja mukaan, jos sellaisia
on tarkasteltavassa muuttujassa. Frekvenssitaulussa ei tarvitse huomioida
mahdollisia puuttuvia alkioita.

Kirjoita funktio summamuuttuja, joka ottaa argumenteikseen:

* tarkasteltavien muuttujien nimet merkkijonovektorina
* aineiston taulukkona eli data.frame:na
* suurimman sallittujen puuttuvien arvojen määrän / vastaaja (aseta argumentin oletusarvoksi 1).

Funktion tulee palauttaa summamuuttuja tarkasteltavista muuttujista,
eli vektori johon on laskettu jokaiselle vastaajalle hänen vastaustensa
keskiarvo tarkasteltavista muuttujista. Käytä keskiarvoja laskiessasi
argumenttia na.rm = TRUE, mutta muuta summamuuttujan
arvot niiden vastaajien, joilta puuttuu vastaus useampaan kysymykseen
kuin mikä on argumentiksi annettu suurin sallittu puuttuvien
vastausten määrä, kohdalta puuttuviksi.

Lataa HSL:n asiakastyytyväisyyskyselyn aineisto taulukkoon asty, ja
laske funktiosi summamuuttuja avulla vastaajien keskimääräinen tyytyyväisyys
kuljettajien toimintaan asteikolla 1-5 muuttujaan tyyt_kulj
laskemalla vastaajakohtaiset keskiarvot muuttujista K1A1, K1A2 ja
K1A3. Määrää funktiosi argumentilla summamuuttujan arvo puuttuvaksi,
jos vastaajalta puuttuu vastaus useampaan kuin yhteen kysymykseen.

Funktiokutsun tulisi siis toimia esimerkiksi seuraavasti:
asty$tyyt_kulj <- summamuuttuja(c(’K1A1’, ’K1A2’, ’K1A3’),
data = asty, max_puuttuvat = 1)

Piirrä summamuuttujan jakaumasta punainen histogrammi. Tarkastele
myös yhteenveto-funktiollasi summamuuttujan tyyt_kulj frekvenssitaulua
ja keskiarvoa ja kerro näiden perusteella miltä vastaajien
keskimääräinen tyyväisyys kuljettajien toimintaan vaikuttaa
suurin piirtein? Muuttujien asteikot ja niiden kuvaukset löytyvät koodikirjasta.
Oletusasetuksilla histogrammista tulee hieman harva. Miten
selität histogrammin pylväiden väliin jäävät raot frekvenssitaulun
avulla? Etsi histogrammifunktion breaks-argumentille sellainen
arvo, että pylväiden väliin ei jää rakoja ja esitä myös tämä uusi histogrammi
mieleiselläsi värillä. Piirrä vielä histogrammiin pystysuora
viiva summamuuttujan keskiarvon kohdalle ja tee viivasta riittävän
paksu (esim. argumentilla lwd=5).

Laske funktiosi summamuuttuja avulla vastaajien keskimääräistä tyytyväisyyttä
HSL:n palveluihin kuvaava summamuuttuja tyyt_hsl
laskemalla vastaajakohtaiset keskiarvot muuttujista K1A4, K1A5, K1A6,
K2A2, K2A3, K2A4, K2A5 ja K2A6. Määrää funktiosi argumentilla summamuuttujan
arvo puuttuvaksi, jos vastaajalta puuttuu vastaus useampaan
kuin viiteen kysymykseen. Tarkastele yhteenveto-funktiollasi
summamuuttujan frekvenssitaulua ja keskiarvoa. Mitä voit näiden 2
perusteella suurin piirtein sanoa vastaajien tyytyväisyydestä HSL:n
palveluihin?

### 2: Osa-aineistoja ja tilastollista hypoteesintestausta ###
Tee osa-aineisto, johon kuuluvat vain vastaajat, jotka ovat ilmoittaneet
postinumerokseen joko 00200 (Lauttasaari), 02100 (Tapiola) tai
02230 (Matinkylä). 1 Rajaa aineistoa siis muuttujan T18 (Postinumero)
avulla ja tallenna osa-aineisto taulukkoon asty_osa. Huomaa,
että postinumeromuuttuja on kokonaislukumuodossa, minkä vuoksi
R tulostaa sen arvot ilman mahdollisia etunollia. Postinumeroa voi
kuitenkin käsitellä joko etunollien kanssa tai ilman. Voit siis esimerkiksi
kirjoittaa joko 00200 tai vain 200. Muunna osa-aineiston muuttuja
T18 nyt faktoriksi, jonka tasot ovat Lauttasaari, Tapiola ja
Matinkyla.

Ristiintaulukoi faktoriksi muuttamasi muuttuja T18 osa-aineiston muuttujan
K3A23 (HSL on hoitanut Länsimetron viivästymisestä aiheutuneet
korvaavat liikennejärjestelyt hyvin) kanssa. Laske taulukosta
joko rivi- tai sarakeprosentit sen mukaan, kummalla niistä on järkevämpi
tulkinta. Voit esittää luvut desimaalilukuina tai prosenttimuodossa,
mutta pyöristä luvut sopivaan esitystarkkuuteen.

Testaa osa-aineistossasi Khiin neliön testillä, onko vastaajan postinumeroalueella
(Lauttasaari, Tapiola, Matinkylä) ja tämän vastauksella
edellä tarkasteltuun muuttujaan K3A23 välillä yhteyttä käyttäen
merkitsevyystasoa α = 0.01. Mikä on testin tulos? Jos muuttujien
välillä on yhteyttä, tulkitse yhteyttä sanallisesti b-kohdan tulosten
avulla.

Kuvitellaan tilanne, että HSL olisi asettanut tavoitteekseen Lauttasaaren
kanssa, että asukkaiden keskimääräinen mielipide Länsimetron
korvaavista liikennejärjestelyistä olisi vähintään 3 (kun asteikko
on 1-5). Testaa osa-aineistossasi yhden otoksen t-testillä 2 nollahypoteesia:

H0 : µLauttasaari ≥ 3.0

yksisuuntaista vastahypoteesia

H1 : µLauttasaari < 3.0

vastaan,

eli sitä onko lauttasaarelaisten keskimääräinen mielipide Länsimetron
korvaavista liikennejärjestelyistä (eli muuttuja K3A23) vä-
hintään tasolla 3. Käytä merkitsevyystasoa α = 0.05. Mikä on testin
tulos ja sen mukainen johtopäätös? Voidaanko testin perusteella katsoa,
että HSL olisi epäonnistunut tässä kuvitteellisessa tavoitteessa?

### 3: Päivämäärien käsittelyä ja luottamusvälien visualisointia ###

Nämä ovat eräitä postinumeroalueita, joihin Länsimetro tulee liikennöimään, jos ja kun se
valmistuu. Lauttasaaressa, Tapiolassa ja Matinkylässä on muitakin postinumeroalueita, mutta
niitä ei oteta tähän mukaan.

Ryhmän otoskoko on suurehko, jolloin ryhmän otoskeskiarvon voidaan olettaa olevan suurin
piirtein normaalijakautunut, mikä mahdollistaa t-testin käyttämisen.

Palataan takaisin käsittelemään koko aineistoa eli taulukkoa asty.
Aineisto on etukäteen rajattu siten, että vastauksia on vain vuosilta
2016 ja 2017. Vastausten päivämäärät löytyvät muuttujasta
PAIVAMAARA muodossa "vvvv-kk-pp". Määritellään nyt vastaajan ikä
vähentämällä vastaajan syntymävuosi vuosiluvusta, jolloin tämä on
vastannut kyselyyn.3 Määritä ensin aineistoon uusi numeerinen muuttuja
vuosi, joka kertoo, minä vuonna kyselyyn vastattiin. Määritä
sitten aineistoon uusi muuttuja ika vähentämällä vuosiluvusta vastaajan
syntymävuosi eli muuttuja T7.

Tee funktio KeskiarvoJaVali, joka ottaa argumentikseen vektorin
lukuja ja halutun luottamustason, ja palauttaa toisen vektorin joka
sisältää funktiolle syötettyjen lukujen keskiarvon, sekä niitä vastaavan
t-luottamusvälin ala- ja ylärajan.

Luokittele vastaajien iät kuuteen luokkaan: Alle 18-vuotiaat, 18-29-
vuotiaat, 30-39-vuotiaat, 40-65-vuotiaat, 66-75-vuotiaat ja yli 75-
vuotiaat. Tallenna ikäluokat aineistoon uuteen muuttujaan ikaluokka.
Tee sitten osa-aineisto, johon valitset vain metrossa kyselyyn vastanneet.
Rajaa aineistoa siis muuttujan LIIKENNEMUOTO avulla. Muuttujan
kuvaukset löytyvät koodikirjasta. Tallenna osa-aineisto taulukkoon
asty_metro. Laske osa-aineistosta funktiota KeskiarvoJaVali
käyttäen muuttujan K1A6 (Matkustusmukavuus (sisätilojen varustus)
on hyvä) keskiarvo ja 95% t-luottamusväli jokaiselle ikäryhmälle.

Visualisoi muuttujan K1A6 (Matkustusmukavuus (sisätilojen varustus)
on hyvä) vaihtelua ikäryhmittäin metrossa kyselyyn vastanneiden
keskuudessa. Piirrä ensin tyhjä kuva ja rajoita sen x-akseli välille
1-6 (ikäryhmien lukumäärä). Käytä kuitenkin argumenttia xaxt="n",
jolloin x-akselin pisteille ei tule mitään kuvauksia, koska haluamme
numeroiden 1-6 sijasta x-akselin pisteille kuvauksiksi ikäryhmien tasot.
Rajoita y-akseli välille 1-5 (K1A6-muttujan vaihteluväli). Anna
x-akselin otsikoksi ”Ikäryhmä” ja y-akselin otsikoksi "Mielipide matkustusmukavuudesta".
Piirrä sitten kuvaan jokaiselle kuudelle ikä-
ryhmälle lasketut muuttujan K1A6 keskiarvot pisteinä. Käytä pisteenä
mieleistäsi merkkiä (argumentti pch). Piirrä jokaisen ikäryhmän
pisteen ympärille näiden luottamusvälejä vastaavat (pystysuorat) janat.
Selvitä, kuinka saat x-akselin pisteiden kuvauksiksi ikäryhmien tasot
(alle 18, 20-29, jne.). Tätä ei ole selitetty kurssimateriaalissa, joten
joudut kaivamaan tiedon muualta. Vihje: funktio axis.
Arvioi silmämääräisesti tai katso edellisestä kohdasta, millä ikäryhmällä
luottamusväli on pisin ja millä se on lyhin. Pohdi sanallisesti,
mistä erot ikäryhmien luottamusvälien pituuksissa johtuvat.

### 4: Muuttujien tutkimista lineaarisella mallilla ###
Lataa YK:n sosiaali-indikaattorit-aineisto taulukkoon yk ja lisää siihen
uusi muuttuja logBKT ottamalla luonnollinen logaritmi asukaskohtaista
bruttokansantuotetta kuvaavasta muuttujasta BKT. Sovita sitten aineistolla lineaarinen malli, jossa selität vuotuista väestönkasvua
(%) vaestonkasvu muuttujan logBKT avulla. Tarkastele
summary-funktion mallille palauttamaa tulostetta. Millainen suhde
väestönkasvulla ja bruttokansantuotteen logaritmilla vaikuttaa tä-
män perusteella olevan?

Visualisoi muuttujien logBKT ja vaestonkasvu välistä suhdetta piirtämällä
niistä hajontakuva. Erota kuvassa eri alueisiin kuuluvat valtiot
määrittämällä pisteiden värit muuttujan alue mukaan ja piirrä
kuvaan regressiosuora.
Etsi hajontakuvasta kaksi havaintoa, jotka poikkeavat mielestäsi eniten
regressiosuorasta. Merkitse kuvaan niitä vastaavien valtioiden nimet
text-funktion avulla.

Lisää lukutaito_naiset lineaariseen malliin toiseksi selittäjäksi. Tarkastele
jälleen summary-funktion tulostetta, ja laske sen lisäksi 90%
luottamusvälit mallin kertoimille. Millainen vaikutus muuttujan
lukutaito_naiset lisäämisellä oli malliin? Mistä arvelisit tämän tuloksen
johtuvan?

### 5: Bayes-päättelyä simuloimalla ###
Pekka on saanut Maijalta kolikon, joka palauttaa heitettynä kruunia eräällä
tuntemattomalla todennäköisyydellä 0 < θ < 1. Pekka haluaa selvittää,
mikä parametrin θ oikea arvo todennäköisesti on, joten hän päättää yrittää
määrittää sitä kokeellisesti heittämällä saamaansa kolikkoa muutamia
kertoja. Kuudella heitolla Pekka havaitsi yhteensä neljä kruunaa.
Tässä havaittujen kruunien lukumäärä Y noudattaa binomijakaumaa otoskoolla
6 ja onnistumistodennäköisyydellä θ, eli Y |θ ∼ Bin(6, θ). Pekalla ei
ole mitään esitietoa parametrin θ arvosta, joten hän päättää olettaa sen
kaikkien mahdollisten arvojen olevan yksinkertaisesti yhtä todennäköisiä.
Toisin sanoen θ:n ajatellaan siis olevan tasajakautunut välillä (0, 1), eli
θ ∼ U(0, 1).

Tutkitaan tilannetta simuloimalla, mitä parametrista θ voidaan
päätellä saadun tuloksen perusteella.

Kirjoita funktio, joka simuloi tehtävän tilannetta. Funktion tulee ottaa
argumenttinaan simulaation otoskoko n ja palauttaa vektori kaikista
generoiduista θ:n arvoista, joilla arvottu kruunien lukumäärä
on tasan neljä.
Toteuta funktio generoimalla ensiksi tasajakaumasta n kappaletta
mahdollisia onnistumistodennäköisyyksiä. Arvo sitten näillä saatujen
kruunien määrät, kun kolikkoa heitetään kuusi kertaa jokaisella
n onnistumistodennäköisyydellä. Valitse lopuksi generoiduista onnistumistodennäköisyyksistä
ne, joilla saatu kruunien lukumäärä vastaa
Pekan saamaa havaintoa Y = 4.
Suorita valmis funktio kertaalleen simulaation otoskoolla n = 100000,
piirrä sen palauttamista arvoista histogrammi ja laske saaduista arvoista
lisäksi tavallisia tunnuslukuja. Mitä voit sanoa parametrin θ
jakaumasta simulaation avulla saadun approksimaation perusteella?

Maija seuraa sivusta Pekan koetta. Hän tietää, että hänen Pekalle
antamansa lantti on yksi neljästä samannäköisestä kolikosta, joilla
on tunnetut onnistumistodennäköisyydet θ1 = 0.3, θ2 = 0.5, θ3 = 0.6
ja θ4 = 0.8. Maija valitsi kolikon sattumanvaraisesti eikä siksi tiedä,
mitä näistä kolikosta Pekka heittää. Pekan tavoin hän päättää täten
olettaa, että kaikki neljä kolikkoa ovat yhtä todennäköisiä.
Asetelmalta tilanne eroaa nyt a)-kohdasta esitiedon osalta. Tällä kertaa
tutkittavan parametrin θ tiedetään noudattavan kolikkojen joukon
{1, 2, 3, 4} diskreettiä tasajakaumaa.
Tee taas funktio, joka simuloi koetta hyödyntäen nyt uutta esitietoa.
Funktion on otettava jälleen argumentikseen simulaation otoskoko n.
Tällä kertaa sen on kuitenkin palautettava havaitut todennäköisyydet
kullekkin eri kolikolle esimerkiksi tauluna tai nimettynä vektorina.
Toteuta funktio arpomalla aluksi n kappaletta kolikoita, ja generoi
niitä vastaavilla onnistumistodennäköisyyksillä kruunien määrät
n:lle kuuden kolikon heitolle. Rajaa arvotuista kolikoista ne, joilla
generoitu kruunien määrä vastaa Pekan havaintoa Y = 4, ja laske
kunkin mahdollisen kolikon 1-4 osuudet tässä osajoukossa.
Suorita valmis funktio jälleen otoskoolla n = 100000. Mitä näistä
neljästä kolikosta Pekka todennäköisimmin heittelee?